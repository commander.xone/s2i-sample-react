FROM node:lts-alpine3.11
RUN mkdir /root/sample
WORKDIR /root/sample

ENV PATH /sample/node_modules/.bin:$PATH

COPY . ./


EXPOSE 3000
CMD ["npm", "start"]
